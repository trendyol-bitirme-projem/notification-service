package com.trendyol.notificationservice.service.notification;

import com.trendyol.notificationservice.integration.UserRestService;
import com.trendyol.notificationservice.model.Notification;
import com.trendyol.notificationservice.model.NotificationType;
import com.trendyol.notificationservice.model.User;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;
@RequiredArgsConstructor
@Service
@Slf4j
public class MailService implements INotificationService {

    @Value("${mail.username:#{null}}")
    private String username;
    @Value("${mail.password:#{null}}")
    private String password;
    @Value("${mail.host:#{null}}")
    private String host;
    @Value("${mail.port:#{null}}")
    private String port;
    private final UserRestService userRestService;

    
    @Override
    public void sendNotifications(List<Notification> notifications) {
        try{
            Map<NotificationType, List<Notification>> groupedByNotificationType = notifications.stream()
                    .collect(Collectors.groupingBy(Notification::getType));

            List<Integer> disctinctUserIds = notifications.stream().map(Notification::getUserId).distinct().collect(Collectors.toList());
            List<User> users = userRestService.getUsers(disctinctUserIds).execute().body();

            for(Notification notification : notifications){

                User user = users.stream().filter(k->k.getId().equals(notification.getUserId())).findFirst().orElse(null);

                if(user != null){
                    String body = getBodyByNotificationType(notification.getType(), notification.getProperties());
                    String subject = getSubjectByNotificationType(notification.getType(), notification.getProperties());

                    sendMail(body, subject, Collections.singletonList(user.getEmail()), new ArrayList<>(), new ArrayList<>());
                }
            }

        }catch (IOException e) {
            log.error(e.getMessage());
        }
    }

    public void sendMail(String body, String subject, List<String> to, List<String> cc, List<String> bcc) {
        Properties props = System.getProperties();
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.user", username);
        if (!StringUtils.isEmpty(password))
            props.put("mail.smtp.password", password);
        props.put("mail.smtp.port", port);
        props.put("mail.smtp.auth", "true");
        Session session = Session.getDefaultInstance(props);
        MimeMessage message = new MimeMessage(session);
        try {
            message.setFrom(new InternetAddress(username));
            if (!CollectionUtils.isEmpty(to)) {
                InternetAddress[] addresses = new InternetAddress[to.size()];
                for(int i = 0; i< to.size(); i++)
                    addresses[i] = new InternetAddress(to.get(i));
                message.addRecipients(Message.RecipientType.TO, addresses);
            }
            if (!CollectionUtils.isEmpty(cc)) {
                InternetAddress[] addresses = new InternetAddress[cc.size()];
                for(int i = 0; i< cc.size(); i++)
                    addresses[i] = new InternetAddress(cc.get(i));
                message.addRecipients(Message.RecipientType.CC, addresses);
            }
            if (!CollectionUtils.isEmpty(bcc)) {
                InternetAddress[] addresses = new InternetAddress[bcc.size()];
                for(int i = 0; i< bcc.size(); i++)
                    addresses[i] = new InternetAddress(bcc.get(i));
                message.addRecipients(Message.RecipientType.BCC, addresses);
            }
            message.setSubject(subject,"utf-8");
            message.setContent(body,"text/html; charset=UTF-8");
            Transport transport = session.getTransport("smtp");
            transport.connect(host, username, password);
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
        } catch (MessagingException e) {
            log.error(e.getMessage(), e);
        }
    }

    private String getBodyByNotificationType(NotificationType type, Map<String, Object> props){
        switch (type){
            case STOCK_IS_LOW:
                return "Fırsatı kaçırmak üzeresin. Hemen uygulamaya girerek ürünü tükenmeden satın al.";
            case PRICE_CHANGED:
                return "Satıcı fiyatı düşürdü. İstediğin ürünü uygun fiyata almak için acele et.";
            case STOCK_IS_FINISH:
                return "Ürünün stoğu tükenmiştir. Favorilerine ekleyerek stok yenilendiğinde alablirsin.";
            default:
                return "c";
        }
    }

    private String getSubjectByNotificationType(NotificationType type, Map<String, Object> props){
        switch (type){
            case STOCK_IS_LOW:
                return "Sepetine Eklediğin Ürün Tükenmek Üzere!";
            case PRICE_CHANGED:
                return "Sepetindeki Ürünün Fiyatı Düştü!";
            case STOCK_IS_FINISH:
                return "Sepetindeki Ürünün Stoğu Tükendi!";
            default:
                return "";
        }
    }
}
