package com.trendyol.notificationservice.service.notification;

import com.trendyol.notificationservice.model.Notification;

import java.util.List;

public interface INotificationService {
    void sendNotifications(List<Notification> notificationLs);
}
