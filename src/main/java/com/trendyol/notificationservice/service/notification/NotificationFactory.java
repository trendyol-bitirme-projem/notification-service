package com.trendyol.notificationservice.service.notification;

import com.trendyol.notificationservice.integration.UserRestService;
import com.trendyol.notificationservice.model.NotificationAction;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class NotificationFactory {

    public final UserRestService userRestService;
    public final MailService mailService;

    public INotificationService getNotificationService(NotificationAction action){
        switch (action){
            case MAIL:
                return mailService;
            case SMS:
                return new SmsService();
            case PUSH:
                return new PushNotificationService();
        }
        return null;
    }
}
