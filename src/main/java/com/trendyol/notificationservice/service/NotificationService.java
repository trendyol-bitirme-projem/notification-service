package com.trendyol.notificationservice.service;

import com.trendyol.notificationservice.model.Notification;
import com.trendyol.notificationservice.model.NotificationAction;
import com.trendyol.notificationservice.service.notification.INotificationService;
import com.trendyol.notificationservice.service.notification.NotificationFactory;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class NotificationService {

    public final NotificationFactory notificationFactory;

    public void sendNotifications(List<Notification> notifications){
        Map<NotificationAction, List<Notification>> groupedNotifications = notifications.stream().collect(Collectors.groupingBy(Notification::getAction));
        groupedNotifications.forEach((k,v)->{
            INotificationService service = notificationFactory.getNotificationService(k);
            service.sendNotifications(v);
        });
    }
}
