package com.trendyol.notificationservice.model;

import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
public class Notification {
    private Integer userId;
    private NotificationAction action;
    private NotificationType type;
    private Map<String, Object> properties = new HashMap<>();
}
