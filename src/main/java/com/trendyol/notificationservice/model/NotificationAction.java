package com.trendyol.notificationservice.model;

import lombok.Getter;

@Getter
public enum NotificationAction {
    MAIL,
    SMS,
    PUSH
}
