package com.trendyol.notificationservice.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class User {
    public Integer id;
    public String userName;
    public String password;
    public String name;
    public String surName;
    public String email;
    public String phoneNumber;
    public String address;
    public Date createDate;
    public Date updateDate;
}
