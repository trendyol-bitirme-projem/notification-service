package com.trendyol.notificationservice.integration;

import com.trendyol.notificationservice.model.User;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

import java.util.List;

public interface UserRestService {
    @GET("/api/user/list")
    Call<List<User>> getUsers(@Query("userIds") List<Integer> userIds);
}
