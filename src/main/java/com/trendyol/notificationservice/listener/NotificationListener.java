package com.trendyol.notificationservice.listener;

import com.trendyol.notificationservice.model.Notification;
import com.trendyol.notificationservice.service.NotificationService;
import lombok.RequiredArgsConstructor;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class NotificationListener {

    public final NotificationService notificationService;

    @KafkaListener(
            topics = "${spring.kafka.topic.notification}",
            groupId = "notification-group",
            clientIdPrefix = "notification-group",
            containerFactory = "notificationQueueKafkaListenerContainerFactory"
    )
    public void listenNotifications(List<Notification> notifications){
        notificationService.sendNotifications(notifications);
    }

}
