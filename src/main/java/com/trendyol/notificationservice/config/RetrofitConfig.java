package com.trendyol.notificationservice.config;

import com.trendyol.notificationservice.integration.UserRestService;
import lombok.RequiredArgsConstructor;
import okhttp3.OkHttpClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.util.concurrent.TimeUnit;

@Configuration
@RequiredArgsConstructor
public class RetrofitConfig {

    @Value("${user-service.url:#{null}}")
    private String userServiceBaseUrl;

    @Bean
    public UserRestService userRestService() {
        OkHttpClient.Builder builder = new OkHttpClient().newBuilder();
        builder.readTimeout(10 * 60, TimeUnit.SECONDS);
        builder.connectTimeout(10 * 60, TimeUnit.SECONDS);
        OkHttpClient client = builder.build();
        return new Retrofit.Builder()
                .baseUrl(userServiceBaseUrl)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build().create(UserRestService.class);
    }
}
